# Overview

This repository gives free access to data and statistical analyses scripts relative to the SISCOVID study, to allow the assessment of reproductibility of results.

This french multicenter observational study aims to describe respiratory sequelae of patients hospitalized for severe SARS-Cov2-induced pneumonia (COVID-19) requiring oxygen supply.

See:

- brief description of protocol on the [ClinicalTrials.gov website](https://clinicaltrials.gov/ct2/show/NCT04505631?term=SISCOVID&draw=2&rank=1),
- [website of the study](https://ghrmsa-drc.fr/etude_SISCOVID/index.html) (in french).

> NB: on this website, configuration files for our OpenClinica-based eCRF are freely available! (at the end of eCRF documentation page)


# Available materials

## Clinical data

De-identified clinical data prospectively recorded for the study.

> June 2022: the article has just been accepted for publication, so data are now available.

### Files

- `shared_data.csv`: clinical data in CSV spreadsheet
- `shared_data_descr.csv`: information on variables:
  - *variable*: name of the variable, used in R scripts
  - *type*: type of the variable (according to types of R objects)
  - *visit*: visit related to the variable. V1=3-month follow-up visit, V2=6-month follow-up visit
  - *description* and *description_en*: description of data contained in the variable, in french and english, respectively
  - *section* and *section_en*: eCRF section related to the variable, in french and english, respectively
  - *subsection* and *subsection_en*: eCRF subsection related to the variable, in french and english, respectively
- `shared_data.RData`: both files above gathered in one archive ready to be used with R. Because it keeps all configuration of dataframes (variable type, labels...), we recommend to use this file rather than importing previous files with *read.csv* function.

### De-identification process

- Limiting data only to those required to run the analysis script (`results.Rmd`)
- Removing potentially identifying data: patient ID in the study (replaced by a numeric sequence randomly allocated), dates, precise morphological features such as height and weight
- Jittering numeric data at risk of identification but necessary for analysis:
  - body mass index: rounding at one decimal
  - length of stays: absolute change from 0 to 2 days, approximately
  - age: absolute change from 0 to 2 years, approximately
  
  > This jittering is likely to impact results, but to a moderate extent, and if you run the script, you may get results slightly different than original version of `results.html`. It's more significant for age, which is used in multivariate models.

## Statistical analyses scripts

All statistical analyses were performed with *R* language, and analysis report were generated using *rmarkdown* with *RStudio* software (version 1.4.1103).

Files:

- `config.R`: configuration, mainly global variables
- `functions.R`: R functions used in main scripts
- `results.Rmd`: *rmarkdown* script for analysis report generation
- `results.html`: analysis report used for publication. Contains information on version of R software and R packages used

Language: english


# How to use this material?

## Reproduce the analyses

1. Clone the repository

2. Adapt configuration in `config.R` if necessary (probably not)

3. Open `results.Rmd` with RStudio, install required packages if necessary (see calls of *library()* function at the begining of the file, line 50 approximately) and *knit* the document.

## Assess the statistical results

Open `results.html` with any web browser and start reading!
